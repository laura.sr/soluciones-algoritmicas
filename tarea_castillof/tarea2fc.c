#include <string.h>
#include <stdio.h>
//invertiremos una frase o palabra ingresada por el usuario
char *invertir(char frase[]); // ocuparemos el char para poder ocupar la cadena 
  
int main(void) { //ocuparemos el int vaid para que se pueda llevar a cabo la funcion 
  char frase[100]; //ocuparemos para la cadena un caracter 
  printf("Usuario porfavor escriba una palabra para poderla invertirla, la longuitud de esta no puede super los %d: ", 100 - 1); // el usuario debe ingresar la palabra poderla invertirla, la cual no puede superar los 399 caracteres 
  gets(frase, 100, stdin); //se guardara lo escrito en la frase gracias al gets y necesitaremos leer 
  

  frase[strcspn(frase, "\r\n")] = 0; // esto se utiliza para remover el salto de linea de la palabra que el usuario ingreso 
 
  printf("La palabra u frase invertida es: %s", invertir(frase)); //nos impimira la palabra o la frase que se quiere invertir 
  return 0; 
}
char *invertir(char frase[]) //usaremos el char para poder invertir la frase que se ingreso por el usuario 
 {
  int longitud = strlen(frase); //tendremos la variable de longuitud y luego esta se podra invertir 
  char invertir_frase; //el caracter de letras sera temporal 
  for (int izquierda = 0, derecha = longitud - 1; izquierda < (longitud / 2); //escribiremos la condicion para poder invertir la frase que se ha ingresado con el siguiente codigo 
       izquierda++, derecha--) {  //escribiremos la secuencia de como invertiremos la cadena 
    invertir_frase = frase[izquierda];  //invertiremos la frase de derecha a izquierda 
    frase[izquierda] = frase[derecha]; //invertiremos la frase de izquierda a derecha 
    frase[derecha] = invertir_frase; //invertiremos de derecha la frase
  }
  return  frase;
}
//se podra ejecutar la accion de dar vuelta la frase o la cadena ingresada 
