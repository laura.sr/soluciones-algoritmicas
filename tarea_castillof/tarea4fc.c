#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//buscaremos la forma de encontrar la repeticion de un caracter en una frase determinada 
 int main ()
 {
	 int contador_frase=0, largo; //declaramos el tipo de variabe el cual sera de tipo entero 
	 char frase_ingresada[100], caracter; //el arreglo constara con un espacio de 100 caracteres 
	 
	 printf("Estimado usuario porfavor ingrese una frase que contenga hasta un maximo de 100 caracteres: "); //el usuario debe ingresar una frase a su criterio 
	 gets(frase_ingresada); // se guardara la frase que el usuario pueda ingresar
	 printf("La frase que fue ingresada es %s\n", frase_ingresada); //imprimiremos nuevamente la frase de la variable 
	 
	 printf("Ingrese  un caracter para poder ver su repeticion\n");//el usuario debe ingresar la frase para poder observar como se repiten la sucesion de caracteres
	 scanf("%c", &caracter); //el usuario debe ingresar el caracter para que este se pueda guardar y leer
	 
	 largo = strlen(frase_ingresada); //veremos el largo por medio de esta opcion de la frase ingresada
	 
	 for (int n=0; n<largo ; n++){ //escribiremos la condicion de for necesaria cuando se ingresa el 
		 if (frase_ingresada[n] == caracter){ // veremos que que si el caracter esta en la frase y se repite mas de una vez el contador suba el valor 
			 contador_frase++; // al contador de la frase se le ingresara un numero mas 
		 }
	 }
	 printf("El numero de veces que aparecera %c es %d\n", caracter, contador_frase); //podremos ver el numero de veces que aparecera el caracter en el contador necesario
	 
	 return 0;
 }
